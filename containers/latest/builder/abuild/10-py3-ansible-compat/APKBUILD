# Contributor: Artem Korezin <source-email@yandex.ru>
# Maintainer: Artem Korezin <source-email@yandex.ru>
pkgname=py3-ansible-compat
_pkgname=ansible_compat
# renovate: datasource=pypi depName=ansible-compat
pkgver=25.1.4
pkgrel=99
pkgdesc='An implementation of JSON Schema validation for Python'
url='https://github.com/ansible/ansible-compat'
arch='noarch'
license='MIT'

# renovate: datasource=repology depName=alpine_3_21/python3 versioning=loose
depends="$depends python3=3.12.9-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-yaml versioning=loose
depends="$depends py3-yaml=6.0.2-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-packaging versioning=loose
depends="$depends py3-packaging=24.2-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-subprocess-tee versioning=loose
depends="$depends py3-subprocess-tee=0.4.2-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-jsonschema versioning=loose
depends="$depends py3-jsonschema=4.23.0-r0"
# renovate: datasource=repology depName=alpine_3_21/ansible-core versioning=loose
depends="$depends ansible-core=2.18.1-r0"

# renovate: datasource=repology depName=alpine_3_21/py3-gpep517 versioning=loose
makedepends="$makedepends py3-gpep517=16-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-setuptools_scm versioning=loose
makedepends="$makedepends py3-setuptools_scm=8.1.0-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-wheel versioning=loose
makedepends="$makedepends py3-wheel=0.43.0-r0"

# renovate: datasource=repology depName=alpine_3_21/py3-pytest versioning=loose
checkdepends="$checkdepends py3-pytest=8.3.4-r0"
# renovate: datasource=repology depName=alpine_3_21/py3-pytest-mock versioning=loose
checkdepends="$checkdepends py3-pytest-mock=3.10.0-r3"

subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname::1}/${_pkgname//_/-}/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options='!check'

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer \
		--destdir="$pkgdir" \
		dist/*.whl
}
sha512sums="
dd5c3d81b52a307069f4bd20dce5adf0e241dc4dcd03e60aca81a456acebeb1f218c74b6eebe6f3fcf2661c7c199688982979c96337b9ecff64e196de8f9011b  ansible_compat-25.1.4.tar.gz
"
