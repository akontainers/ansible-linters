#!/usr/bin/env python3
"""
Simple j2 linter, useful for checking jinja2 template syntax.

@author Gerard van Helden <drm@melp.nl>
@license DBAD, see <http://www.dbad-license.org/>
"""
import os
import sys
from functools import reduce
from jinja2 import BaseLoader, TemplateNotFound, Environment, exceptions


class AbsolutePathLoader(BaseLoader):  # pylint: disable-msg=missing-class-docstring
    def get_source(self, _environment, template):  # noqa: D102
        if not os.path.exists(template):
            raise TemplateNotFound(template)
        mtime = os.path.getmtime(template)
        with open(template) as file:
            source = file.read()
        return source, template, lambda: mtime == os.path.getmtime(template)


def check(  # pylint: disable-msg=missing-function-docstring
    template,
    out,
    err,
    env=Environment(  # nosec: B701
        loader=AbsolutePathLoader(),
        extensions=[
            "jinja2.ext.i18n",
            "jinja2.ext.do",
            "jinja2.ext.loopcontrols",
        ],
    ),
):
    try:
        env.get_template(template)
        out.write("%s: Syntax OK\n" % template)
        return 0
    except TemplateNotFound:
        err.write("%s: File not found\n" % template)
        return 2
    except exceptions.TemplateSyntaxError as ex:
        err.write(
            "%s: Syntax check failed: %s in %s at %d\n"
            % (template, str(ex), ex.filename, ex.lineno)
        )
        return 1


def main(**kwargs):  # pylint: disable-msg=missing-function-docstring
    try:
        sys.exit(
            reduce(
                lambda r, fn: r + check(fn, sys.stdout, sys.stderr, **kwargs),
                sys.argv[1:],
                0,
            )
        )
    except IndexError:
        sys.stdout.write("Usage: j2lint.py filename [filename ...]\n")


if __name__ == "__main__":
    main()
