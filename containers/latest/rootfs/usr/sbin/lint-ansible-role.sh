#!/usr/bin/env sh

set -eu

use_ansible_lint() {
  rolePath=$1
  echo
  echo "Use ansible-lint"
  ansible-lint "${rolePath}"
}

use_jinja2_lint() {
  rolePath=$1
  echo
  echo "Use ansible-jinja-lint"
  files=$(find "${rolePath}" -type f -name '*.j2')
  for file in $files; do
    echo "Lint file ${file}"
    ansible-jinja-lint "${file}"
  done
}

main() {
  rolePath=$1
  echo
  echo "Lint role ${rolePath}"
  status=0
  use_ansible_lint "${rolePath}" || status=1
  use_jinja2_lint "${rolePath}" || status=1
  return ${status}
}

main "$@"
